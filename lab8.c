#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>
#define MSGSZ     4

typedef struct msgbuf
{
    long type;
    float expected_value;
} message_buf;

int main(int argc, char *argv[])
{
    int msqid, msgflg = IPC_CREAT | 0666, num_mas = argc - 1;
    key_t key;
    pid_t PidsProcess[num_mas];
    if ((msqid = msgget(key, msgflg)) < 0) {
        printf("Ошибка при создании очереди!\n");
        exit(1);
    }
    for (int i = 0; i < num_mas; i++) {
        PidsProcess[i] = fork();
        if (-1 == PidsProcess[i]) {
            printf("Ошибка! Процесс %d не создан!\n", PidsProcess[i]);
            exit(1);
        } else if (0 == PidsProcess[i]) {
            int fp, num_elem;
            float expected_value = 0;
            message_buf in_buf;
            if ((fp = fopen(argv[i + 1], "r")) == NULL) {
                printf("Ошибка при открытии файла!\n");
                exit(1);
            }
            fscanf(fp, "%d", &num_elem);
            float elems[num_elem];
            for (int i = 0; i < num_elem * 2; i = i + 2) {
                fscanf(fp, "%f", &elems[i]);
                fscanf(fp, "%f", &elems[i + 1]);
                expected_value += elems[i] * elems[i + 1];
            }
            printf("Процесс-потомок №%d отправил данные о мат. ожидании в очередь %f\n", i + 1, expected_value);
            in_buf.type = 1;
            in_buf.expected_value = expected_value;
            if (msgsnd(msqid, &in_buf, MSGSZ, IPC_NOWAIT) < 0) {
                perror("Ошибка при отправке сообщения в очередь!\n");
                exit(1);
            }
            exit(0);
        }
    }
    int stat, status;
    message_buf out_buf;
    for (int i = 0; i < num_mas; i++) {
        status = waitpid(PidsProcess[i], &stat, 0);
        if (PidsProcess[i] == status) {
            printf("Процесс-потомок №%d закончил работу,  результат=%d\n", i + 1, WEXITSTATUS(stat));
        }
    }
    for (int i = 0; i < num_mas; i++) {
        if (msgrcv(msqid, &out_buf, MSGSZ, 1, 0) < 0) {
            perror("Ошибка при получении сообщения из очереди!\n");
            exit(1);
        }
        printf("Родитель получил сообщение из очереди: %f\n", out_buf.expected_value);
    }
    msgctl(msqid, IPC_RMID, NULL);
}